package tk.anonlinker.web;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import tk.anonlinker.domain.Link;

/**
 * = LinksItemJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Link.class, type = ControllerType.ITEM)
@RooJSON
public class LinksItemJsonController {
}
