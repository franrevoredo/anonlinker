package tk.anonlinker.web;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import tk.anonlinker.domain.Link;

/**
 * = LinksItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Link.class, type = ControllerType.ITEM)
@RooThymeleaf
public class LinksItemThymeleafController {
}
