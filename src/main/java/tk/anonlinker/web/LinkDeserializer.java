package tk.anonlinker.web;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import tk.anonlinker.domain.Link;
import tk.anonlinker.service.api.LinkService;

/**
 * = LinkDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Link.class)
public class LinkDeserializer extends JsonObjectDeserializer<Link> {

    /**
     * TODO Auto-generated field documentation
     *
     */
    public LinkService linkService;

    /**
     * TODO Auto-generated field documentation
     *
     */
    public ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param linkService
     * @param conversionService
     */
    @Autowired
    public LinkDeserializer(LinkService linkService, ConversionService conversionService) {
        this.linkService = linkService;
        this.conversionService = conversionService;
    }
}
