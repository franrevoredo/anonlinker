package tk.anonlinker.web;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import tk.anonlinker.domain.Link;

/**
 * = LinkJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Link.class)
public abstract class LinkJsonMixin {
}
