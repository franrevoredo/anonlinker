package tk.anonlinker.repository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;
import tk.anonlinker.domain.Link;

/**
 * = LinkRepositoryCustom
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Link.class)
public interface LinkRepositoryCustom {
}
