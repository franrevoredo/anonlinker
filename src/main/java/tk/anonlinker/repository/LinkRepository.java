package tk.anonlinker.repository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import tk.anonlinker.domain.Link;

/**
 * = LinkRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Link.class)
public interface LinkRepository {
}
