package tk.anonlinker.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import tk.anonlinker.domain.Link;

/**
 * = LinkRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */ 
@RooJpaRepositoryCustomImpl(repository = LinkRepositoryCustom.class)
public class LinkRepositoryImpl extends QueryDslRepositorySupportExt<Link> {

    /**
     * TODO Auto-generated constructor documentation
     */
    LinkRepositoryImpl() {
        super(Link.class);
    }
}