package tk.anonlinker.domain;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.jpa.annotations.entity.RooJpaEntity;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * = Link
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJavaBean
@RooToString
@RooJpaEntity
public class Link {

    /**
     * TODO Auto-generated field documentation
     *
     */
    @NotNull
    private String url;

    /**
     * TODO Auto-generated field documentation
     *
     */
    @NotNull
    private String hash;

    /**
     * TODO Auto-generated field documentation
     *
     */
    @NotNull
    private String mail;

    /**
     * TODO Auto-generated field documentation
     *
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Calendar creationDate;

    /**
     * TODO Auto-generated field documentation
     *
     */
    private String ipaddr;

    /**
     * TODO Auto-generated field documentation
     *
     */
    private String comment;
}
