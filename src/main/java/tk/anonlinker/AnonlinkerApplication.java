package tk.anonlinker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * = AnonlinkerApplication
 *
 * TODO Auto-generated class documentation
 *
 */
@SpringBootApplication
public class AnonlinkerApplication {

    /**
     * TODO Auto-generated method documentation
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(AnonlinkerApplication.class, args);
    }
}